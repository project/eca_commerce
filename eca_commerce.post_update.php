<?php

/**
 * @file
 * Post update functions for ECA Commerce.
 */

/**
 * Rename used tokens in available ECA models for ECA 2.0.0.
 */
function eca_commerce_post_update_rename_tokens_2_0_0(): void {
  $tokenNames = [
    'commerce_store_plugin' => 'commerce_tax_plugin',
    'commerce_store_zones' => 'commerce_tax_zones',
    'profile' => 'customer_profile',
  ];
  _eca_post_update_token_rename($tokenNames);
}
